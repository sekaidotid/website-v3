# SEKAI.ID Website V3.2

🎉🎉🎉 SEKAI.ID Website Version 3.2 🎉🎉🎉

- [Next.js](https://nextjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Chakra UI](https://chakra-ui.com/)
- [Framer Motion](https://www.framer.com/motion/)
- [AVIF](https://aomediacodec.github.io/av1-avif/)

## Rekomendasi sistem

Node.js 18 / LTS terbaru

## jika belum mengaktifkan Yarn

```bash
corepack enable
```

## instalasi

```bash
git clone https://gitlab.com/sekaidotid/website-v3.git
```

```bash
cd website-v3
```

```bash
cp .env.example .env
```

```bash
yarn
```

```bash
yarn prepare
```

## deploy

```bash
yarn build
```

publish directory : out
