// import "../stylesheets/html.css";

import * as React from "react";

import {
  Box,
  Center,
  ChakraProvider,
  extendTheme,
  useColorModeValue,
} from "@chakra-ui/react";

import ComponentAdsense from "@/components/adsense";
import { AppProps } from "next/app";
import ComponentFooter from "@/components/footer";
import ComponentNavbar from "@/components/navbar";
import ComponentSirah from "@/components/sirah";
import ComponentScripts from "@/components/scripts";

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <ChakraProvider resetCSS={true}>
      <div>
        <ComponentSirah />
        <ComponentScripts />
        <Box
          w={"100%"}
          // boxShadow={"2xl"}
          // rounded={"xl"}
          p={6}
          m={4}
          // overflow={"hidden"}
          // bg={useColorModeValue("gray.200", "gray.700")}
        >
          <ComponentAdsense />
          <ComponentNavbar />
          <ComponentAdsense />
          <Component {...pageProps} />
          <ComponentAdsense />
          <ComponentFooter />
        </Box>
      </div>
    </ChakraProvider>
  );
};

export default App;
