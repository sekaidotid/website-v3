import * as React from "react";

import {
  Avatar,
  Box,
  Center,
  Grid,
  GridItem,
  Heading,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { GetStaticProps, NextPage } from "next";

import Image from "next/image";
import Link from "next/link";
import { NextSeo } from "next-seo";
import { Post } from "../types";
import { client } from "@/components/cms";
import siteConfig from "../../site-config";
import { motion } from "framer-motion";

interface BlogPageProps {
  posts: Post[];
}

export const getStaticProps: GetStaticProps<BlogPageProps> = async () => {
  const data = await client.request(/* GraphQL */ `
    {
      postCollection(order: date_DESC) {
        items {
          title
          slug
          metaDescription
          featuredImage {
            url
          }
          date
          category {
            title
          }
          author {
            name
            photo {
              url
            }
          }
        }
      }
    }
  `);

  return {
    props: {
      posts: data.postCollection.items,
    },
    // revalidate: 60,
  };
};

const BlogPage: NextPage<BlogPageProps> = ({ posts }) => {
  return (
    <div>
      <NextSeo
        title={siteConfig.title}
        description={siteConfig.description}
        openGraph={{
          type: "website",
          locale: siteConfig.locale,
          title: siteConfig.title,
          images: [
            {
              url: `${siteConfig.url}/social.avif`,
              width: 640,
              height: 360,
              alt: siteConfig.title,
            },
          ],
          url: siteConfig.url,
          site_name: siteConfig.title,
        }}
        twitter={{
          cardType: "summary_large_image",
          handle: "@" + siteConfig.twitterUsername,
          site: "@" + siteConfig.twitterUsername,
        }}
      />
      <div data-testid="Posting">
        <Box>
          <Grid
            templateColumns={{
              base: "1fr",
              md: "repeat(2, 1fr)",
              xl: "repeat(3, 1fr)",
              "2xl": "repeat(4, 1fr)",
            }}
            gap={5}
          >
            {posts.map((post, i) => (
              // eslint-disable-next-line react/jsx-key
              <Box>
                <div key={i}>
                  <Link href={post.slug}>
                    <GridItem colSpan={1}>
                      <motion.button
                        whileHover={{ scale: 1.1 }}
                        whileTap={{ scale: 0.9 }}
                      >
                        <Box
                          width={"100%"}
                          h={"450px"}
                          w={"full"}
                          bg={useColorModeValue("gray.200", "gray.700")}
                          boxShadow={"2xl"}
                          rounded={"md"}
                          p={4}
                          overflow={"hidden"}
                        >
                          <Box
                            h={"270px"}
                            mt={-6}
                            mx={-6}
                            mb={2}
                            pos={"relative"}
                          >
                            <Image
                              src={
                                post.featuredImage.url +
                                "?fit=fill&fm=avif&w=480&h=270&q=30"
                              }
                              alt={post.title}
                              // width={480}
                              // height={270}
                              fill={true}
                            />
                          </Box>
                          <Stack>
                            <Text
                              color={"red.500"}
                              textTransform={"uppercase"}
                              fontWeight={800}
                              fontSize={"sm"}
                              letterSpacing={1.1}
                            >
                              {post.category.title}
                            </Text>
                            <Heading
                              color={useColorModeValue("gray.700", "gray.200")}
                              fontSize={"xl"}
                              noOfLines={2}
                            >
                              {post.title}
                            </Heading>
                          </Stack>
                          <Stack
                            mt={6}
                            direction={"row"}
                            spacing={4}
                            // align={"center"}
                          >
                            {post.author.photo == null ? (
                              <Avatar src="/user.avif" />
                            ) : (
                              <Avatar src={post.author.photo.url} />
                            )}
                            <Stack
                              direction={"column"}
                              spacing={0}
                              fontSize={"sm"}
                            >
                              <Text fontWeight={600}>{post.author.name}</Text>
                              <Text color={"gray.500"}>
                                {post.date.substring(0, 10)}
                              </Text>
                            </Stack>
                          </Stack>
                        </Box>
                      </motion.button>
                    </GridItem>
                  </Link>
                </div>
              </Box>
            ))}
          </Grid>
        </Box>
      </div>
    </div>
  );
};

export default BlogPage;
