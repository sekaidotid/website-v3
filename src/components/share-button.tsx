import {
  Box,
  Center,
  HStack,
  Heading,
  Text,
  VStack,
  useColorModeValue,
} from "@chakra-ui/react";

import Image from "next/image";
import Link from "next/link";
import { NextSeo } from "next-seo";
import {
  FaFacebook,
  FaTwitter,
  FaPinterest,
  FaLinkedin,
  FaTelegram,
} from "react-icons/fa";
import siteConfig from "~/site-config";
import { usePathname } from "next/navigation";

export default function ComponentShareButton() {
  return (
    <Box>
      <Center>
        <Box
          w={"full"}
          boxShadow={"xl"}
          rounded={"xl"}
          p={4}
          m={2}
          overflow={"hidden"}
          bg={useColorModeValue("gray.100", "gray.900")}
        >
          Bagikan
          <HStack spacing={"20px"}>
            <Box>
              <a
                className="inline-block px-4"
                href={
                  "https://www.facebook.com/sharer.php?u=" +
                  siteConfig.url +
                  siteConfig.url +
                  usePathname()
                }
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaFacebook className="social" title="Bagikan Ke Facebook" />
              </a>
            </Box>
            <Box>
              <a
                className="inline-block px-4"
                href={
                  "https://twitter.com/intent/tweet?url=" +
                  siteConfig.url +
                  usePathname() +
                  // "&text=" +
                  // post.title +
                  "&via=" +
                  siteConfig.twitterUsername
                }
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaTwitter className="social" title="Bagikan Ke Twitter" />
              </a>
            </Box>
            <Box>
              <a
                className="inline-block px-4"
                href={
                  "https://www.pinterest.com/pin/create/button?url=" +
                  siteConfig.url +
                  usePathname()
                  // +
                  // "&media=" +
                  // post.featuredImage +
                  // "&description=" +
                  // post.title
                }
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaPinterest className="social" title="Bagikan Ke Pinterest" />
              </a>
            </Box>
            <Box>
              <a
                className="inline-block px-4"
                href={
                  "https://www.linkedin.com/shareArticle?url=" +
                  siteConfig.url +
                  usePathname()
                  // +
                  // "&title=" +
                  // post.title
                }
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaLinkedin className="social" title="Bagikan Ke Linkedin" />
              </a>
            </Box>
            <Box>
              <a
                className="inline-block px-4"
                href={
                  "https://telegram.me/share/url?url=" +
                  siteConfig.url +
                  usePathname()
                  // +
                  // "&text=" +
                  // post.title
                }
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaTelegram className="social" title="Bagikan Ke Telegram" />
              </a>
            </Box>
          </HStack>
        </Box>
      </Center>
    </Box>
  );
}
